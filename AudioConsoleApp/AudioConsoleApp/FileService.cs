﻿using Microsoft.WindowsAPICodePack.Shell;
using System.IO;

namespace AudioConsoleApp
{
    public class FileService
    {
        public void MoveLowQualitySongs(string fromFolder, string toFolder)
        {
            var directories = Directory.GetDirectories(fromFolder);
            var smallerBitrateFiles = new List<Tuple<string, string>>();
            var count = 0;
            var allCount = 0;
            long fileSizeInMb = 0;
            foreach (var directory in directories)
            {
                var files = Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories);
                foreach (var file in files)
                {
                    var f = ShellFile.FromFilePath(file);
                    var props = f.Properties;

                    var bitrate = f.Properties.GetProperty("System.Audio.EncodingBitrate");
                    if (bitrate != null)
                    {
                        if (bitrate.ValueAsObject != null)
                        {
                            var bt = bitrate.ValueAsObject.ToString();
                            if (bt != null)
                            {
                                if (Convert.ToInt32(bt) < 128000)
                                {
                                    var filesize = file.GetFileSize();
                                    long fsinMb = filesize / (1024 * 1024);
                                    fileSizeInMb += fsinMb;
                                    var t = Tuple.Create(file, bt);
                                    smallerBitrateFiles.Add(t);
                                    count++;
                                    File.Move(file, toFolder + "\\" + file.GetFileName(), true);

                                    var st = string.Join(Environment.NewLine, smallerBitrateFiles);
                                    Console.WriteLine("File moved: " + st);
                                }
                            }
                        }
                    }
                    allCount++;
                }
            }

            Console.WriteLine("All moved files in MB: " + fileSizeInMb);
            Console.WriteLine("All files: " + allCount);
            Console.WriteLine("Moved files: " + count);
        }

        public void FindDuplicates(string fromFolder)
        {
            var directories = Directory.GetDirectories(fromFolder);
            var allFiles = new List<string>();
            long duplicatesCount = 0;
            long duplicatesFileSize = 0;
            foreach (var d in directories)
            {
                var files = Directory.GetFiles(d, "*.*", SearchOption.AllDirectories);
                allFiles.AddRange(files);
            }
            var pathsArray = allFiles.ToArray();
            var infos = pathsArray.GetFileInfos();
            var queryDuplicates = infos.QueryDuplicates(fromFolder.Length);
            foreach(var info in queryDuplicates)
            {
                if (info.Key.Contains(".jpg") || info.Key.Contains(".png") || info.Key.Contains(".txt"))
                    continue;

                Console.WriteLine(info.Key + " duplicates");
                if(info != null)
                {
                    var filePath = fromFolder + "\\" + info.FirstOrDefault();
                    var fileSize = filePath.GetFileSizeInMb();
                    duplicatesFileSize += fileSize;
                    File.Delete(filePath);
                    Console.WriteLine($"{filePath}: {fileSize} deleted");
                    foreach(var inf in info)
                    {

                        Console.WriteLine(inf);
                    }
                    duplicatesCount++;
                }
            }
            Console.WriteLine("Deleted " + duplicatesCount + " duplicates.");
            Console.WriteLine("Total: " + duplicatesFileSize + " MB.");
            Console.ReadLine();
        }
    }
}
