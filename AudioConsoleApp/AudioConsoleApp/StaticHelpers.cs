﻿namespace AudioConsoleApp
{
    public static class StaticHelpers
    {
        public static long GetFileSize(this string filePath)
        {
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath).Length;
            }
            return 0;
        }

        public static long GetFileSizeInMb(this string filePath)
        {
            if (File.Exists(filePath))
            {
                var fileSize = new FileInfo(filePath).Length;
                long fsinMb = fileSize / (1024 * 1024);
                return fsinMb;
            }
            return 0;
        }

        public static string GetFileName(this string filePath)
        {
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath).Name;
            }
            return "";
        }

        public static FileInfo GetFileInfo(this string filePath)
        {
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath);
            }
            return null;
        }

        public static IEnumerable<FileInfo> GetFileInfos(this string[] filePaths)
        {
            var list = new List<FileInfo>();    
            foreach(var filePath in filePaths) 
            { 
                if (File.Exists(filePath))
                {
                    list.Add(new FileInfo(filePath));
                }
            }
            return list;
        }


        public static IEnumerable<IGrouping<string, string>> QueryDuplicates(this IEnumerable<FileInfo?> fileList, int charsToSkip)
        {
            // var can be used for convenience with groups.  
            var queryDupNames =
                from file in fileList
                group file.FullName.Substring(charsToSkip) by file.Name into fileGroup
                where fileGroup.Count() > 1
                select fileGroup;

            return queryDupNames;
        }
    }
}
